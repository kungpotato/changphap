export const genRoute = [
  { routeName: 'ค้นหาช่างภาพ', path: '/browse' },
  //{ routeName: 'โปรไฟล์', path: '/profile' },
  { routeName: 'บทความ', path: '/content' },
  { routeName: 'สมัครช่างภาพ', path: '/register' },
  { routeName: 'เข้าสู่ระบบ', path: '/signin' },
]
export const userRoute = [
  { routeName: 'ค้นหาช่างภาพ', path: '/browse' },
  //{ routeName: 'โปรไฟล์', path: '/profile' },
  { routeName: 'บทความ', path: '/content' },
  { routeName: 'โปรไฟล์', path: '/profile' },
  { routeName: 'ออกจากระบบ', path: '/signout' },
]
