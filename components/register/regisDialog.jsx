import React, { forwardRef } from 'react'
import {
  Dialog,
  AppBar,
  Container,
  Toolbar,
  IconButton,
  Typography,
  Button,
  Grid,
  TextField,
  Slide,
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import { DatePicker } from '@material-ui/pickers'
import { makeStyles } from '@material-ui/styles'
import { addNewUser, getUserByEmail } from '../../services/userService'
import { createUserLocal } from '../../auth/authen'
import * as Yup from 'yup'
import { Formik } from 'formik'
import routeType from '../../redux/actions/routeType'

const useStyle = makeStyles(theme => {
  //console.log(theme.palette.primary)
  return {
    appBar: {
      position: 'relative',
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1,
    },
  }
})

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

const fields = [
  { label: 'ชื่อผู้ใช้', name: 'username', type: 'text' },
  { label: 'รหัสผ่าน', name: 'password', type: 'password' },
  { label: 'ยืนยันระหัสผ่าน', name: 'confirmPassword', type: 'password' },
  { label: 'ชื่อ', name: 'firstName', type: 'text' },
  { label: 'นามสกุล', name: 'lastName', type: 'text' },
  { label: 'อีเมลล์', name: 'email', type: 'text' },
  { label: 'เบอร์โทรศัพท์', name: 'phone', type: 'number' },
  // { label: 'วันเกิด', name: 'birthday', type: 'date' },
  //{ label: 'ที่อยู่', name: 'address', type: 'text' },
]

const RegisDialog = props => {
  const { open, isOpen, handleClose, userType, dispatch } = props
  // console.log('RegisDialog', props)
  const classes = useStyle()
  const inputs = {
    username: '',
    password: '',
    confirmPassword: '',
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
  }

  // const [selectedDate, setSelectedDate] = useState(null)

  // const handleDateChange = date => {
  //   setSelectedDate(date)
  // }

  const handleSave = data => {
    // e.preventDefault()
    // console.log('param', data)
    getUserByEmail(data.email).then(res => {
      if (res) {
        console.log('email นี้ มีการใช้งานแล้ว')
      } else {
        addNewUser(
          data.username,
          data.password,
          data.firstName,
          data.lastName,
          data.email,
          data.phone,
          userType
        ).then(uid => {
          // console.log('result', result)
          createUserLocal(data.email, data.password)
          dispatch({
            type: routeType.USE_USER_ROUTE,
          })
          dispatch({
            type: routeType.GOTO_ROUTE,
            payload: `/profile?photographId=${uid}`,
          })
          isOpen()
        })
      }
    })
  }

  return (
    <Dialog
      fullScreen
      open={open}
      onClose={() => handleClose(false)}
      TransitionComponent={Transition}
    >
      <AppBar className={classes.appBar}>
        <Container maxWidth='lg'>
          <Toolbar>
            <IconButton
              edge='start'
              style={{ color: '#fff' }}
              onClick={() => handleClose(false)}
              aria-label='Close'
            >
              <CloseIcon />
            </IconButton>
            <Typography
              style={{ color: '#fff' }}
              variant='h6'
              className={classes.title}
            >
              กรุณากรอกข้อมูล
            </Typography>
          </Toolbar>
        </Container>
      </AppBar>
      <Container maxWidth='lg' style={{ marginTop: '20px' }}>
        <Grid container spacing={3} justify='space-around'>
          <Grid item xs={12} sm={4}>
            <Formik
              render={props => <InputFrom {...props} />}
              initialValues={inputs}
              validationSchema={validationSchema}
              onSubmit={handleSave}
            />
          </Grid>
        </Grid>
        <Typography variant='body1'></Typography>
      </Container>
    </Dialog>
  )
}

const validationSchema = Yup.object({
  username: Yup.string()
    .max(30, 'คุณใส่ตัวอักษรยาวเกินกำหนด')
    .required('กรูณาใส่ชื่อผู้ใช้'),
  firstName: Yup.string()
    .max(30, 'คุณใส่ตัวอักษรยาวเกินกำหนด')
    .required('กรูณาใส่ชื่อ'),
  lastName: Yup.string()
    .max(30, 'คุณใส่ตัวอักษรยาวเกินกำหนด')
    .required('กรูณาใส่นามสกุล'),
  phone: Yup.number()
    .max(15, 'คุณใส่ตัวเลขยาวเกินกำหนด')
    .required('กรูณาใส่เบอร์โทรศัพท์'),
  email: Yup.string()
    .email('คุณใส่ email ไม่ถูกต้อง')
    .max(30, 'คุณใส่ตัวอักษรยาวเกินกำหนด')
    .required('กรูณาใส่ email'),
  password: Yup.string('')
    .min(6, 'รหัสผ่านต้องมีอย่างน้อย 8 ตัวอักษร')
    .max(30, 'คุณใส่ตัวอักษรยาวเกินกำหนด')
    .required('กรูณาใส่รหัสผ่าน'),
  confirmPassword: Yup.string()
    .required('กรูณายืนยันรหัสผ่าน')
    .max(30, 'คุณใส่ตัวอักษรยาวเกินกำหนด')
    .oneOf([Yup.ref('password')], 'รหัสผ่านไม่ตรงกัน'),
})

const InputFrom = props => {
  const {
    handleSubmit,
    values,
    handleChange,
    handleBlur,
    handleDateChange,
    errors,
  } = props
  //console.log('InputFrom', props)

  return (
    <form onSubmit={handleSubmit}>
      <Button
        style={{ float: 'right', color: '#fff' }}
        type='submit'
        variant='contained'
        color='primary'
      >
        save
      </Button>
      {fields.map(item => {
        return item.name !== 'address' && item.name !== 'birthday' ? (
          <TextField
            key={item.name}
            label={item.label}
            name={item.name}
            type={item.type}
            value={values[item.name]}
            onChange={handleChange}
            helperText={errors[item.name]}
            error={!!errors[item.name]}
            onBlur={handleBlur}
            fullWidth
            margin='normal'
            variant='outlined'
          />
        ) : item.name === 'birthday' ? (
          <DatePicker
            key={item.name}
            label={item.name}
            name={item.name}
            fullWidth
            margin='normal'
            variant='dialog'
            inputVariant='outlined'
            // value={selectedDate}
            onChange={handleDateChange}
            format='dd/MM/yyyy'
            views={['date', 'month', 'year']}
          />
        ) : (
          <TextField
            key={item.name}
            label={item.name}
            name={item.name}
            fullWidth
            margin='normal'
            multiline={true}
            rows={3}
            rowsMax={3}
            variant='outlined'
          />
        )
      })}
    </form>
  )
}

export default RegisDialog
