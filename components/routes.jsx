import React from 'react'
import { Button, makeStyles, MenuItem } from '@material-ui/core'
import { withRouter } from 'next/router'
import { connect } from 'react-redux'
import Link from 'next/link'
import routeType from '../redux/actions/routeType'

const useStyles = makeStyles({
  menu: {
    color: '#fff',
    fontWeight: 'normal',
    paddingRight: '10px',
    fontFamily: 'Mitr',
  },
  menu2: {
    color: '#48BFA3',
    fontWeight: 'normal',
    paddingRight: '10px',
    fontFamily: 'Mitr',
  },
  sideMenu: {
    color: '#404040',
    fontWeight: 'normal',
    paddingRight: '10px',
    float: 'left',
  },
})

const Routes = props => {
  const {
    menuType,
    router: { pathname },
    close,
    dispatch,
    routeReducer: { RouterArr },
    userReducer: { uid, displayName },
  } = props
  // console.log('Routes', props)
  const classes = useStyles()

  const handleClick = path => () => {
    dispatch({
      type: routeType.GOTO_ROUTE,
      payload: path,
    })
  }

  return (
    <div className='routes'>
      <React.Fragment>
        {menuType === 'logo' ? (
          // ส่วนของ logo
          <Link href='/'>
            <Button className={classes.menu} onClick={handleClick('home', '/')}>
              <img
                src={
                  pathname === '/' || pathname === '/profile'
                    ? './static/images/logo1.png'
                    : pathname === '/profile/updateProfile'
                    ? '../static/images/logo2.png'
                    : './static/images/logo2.png'
                }
                alt=''
                style={{
                  width: '150px',
                  cursor: 'pointer',
                  marginBottom: '10px',
                }}
              />
            </Button>
          </Link>
        ) : (
          <div>
            {RouterArr.map(item =>
              // ส่วนของ main menu
              menuType == 'main' ? (
                <Link
                  href={
                    uid && item.path === '/profile'
                      ? `${item.path}?photographId=${uid}`
                      : `${item.path}`
                  }
                  key={item.routeName}
                >
                  <Button
                    style={{
                      color:
                        pathname === '/' || pathname === '/profile'
                          ? '#fff'
                          : '#404040',
                    }}
                    className={
                      menuType == 'main' ? classes.menu2 : classes.sideMenu
                    }
                  >
                    {item.routeName}
                  </Button>
                </Link>
              ) : (
                // ส่วนของ menu responsive
                <MenuItem key={item.routeName} onClick={close}>
                  <Link
                    href={
                      uid && item.path === '/profile'
                        ? `${item.path}?photographId=${uid}`
                        : `${item.path}`
                    }
                    key={item.routeName}
                  >
                    <Button
                      className={
                        menuType == 'main' ? classes.menu : classes.sideMenu
                      }
                    >
                      {item.routeName}
                    </Button>
                  </Link>
                </MenuItem>
              )
            )}
          </div>
        )}
      </React.Fragment>
    </div>
  )
}

export default connect(state => state)(withRouter(Routes))
