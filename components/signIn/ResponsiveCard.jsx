import React from 'react'
import { makeStyles } from '@material-ui/styles'
import Card from '@material-ui/core/Card'

const useStyles = makeStyles(theme => {
  return {
    root: {
      [theme.breakpoints.down('sm')]: {
        boxShadow: theme.shadows[0],
      },
      [theme.breakpoints.up('sm')]: {
        width: 500,
        boxShadow: theme.shadows[2],
      },
      height: '100%',
      padding: '20px',
    },
  }
})

const ResponsiveCard = props => {
  const { children } = props
  const classes = useStyles()
  return <Card className={classes.root}>{children}</Card>
}

export default ResponsiveCard
