import React from 'react'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => {
  return {
    root: {
      [theme.breakpoints.up('sm')]: {
        marginTop: '5%',
      },
    },
  }
})

const ResponsiveContainerGrid = props => {
  const { children } = props
  const classes = useStyles()

  return (
    <Grid
      className={classes.root}
      container
      direction='row'
      justify='center'
      align='center'
    >
      {children}
    </Grid>
  )
}

export default ResponsiveContainerGrid
