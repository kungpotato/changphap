import React, { useEffect } from 'react'
import LastProfile from './LastProfile'
import CategoryHome from './CategoryHome'
import ReviewHome from './ReviewHome'
import HowtoWork from './HowtoWork'

const HomeSec2 = () => {
  useEffect(() => {}, [])

  return (
    <React.Fragment>
      <HowtoWork />
      <LastProfile />
      <CategoryHome />
      <ReviewHome />
    </React.Fragment>
  )
}

export default HomeSec2
