const userType = {
  SET_NAME_AND_UID: 'SET_NAME_AND_UID',
  RESET_USER: 'RESET_USER',
}
export default userType
