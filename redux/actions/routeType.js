const routeType = {
  RESET_ROUTE: 'RESET_ROUTE',
  USE_USER_ROUTE: 'USE_USER_ROUTE',
  GOTO_ROUTE: 'GOTO_ROUTE',
}

export default routeType
