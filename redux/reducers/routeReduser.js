import Router from 'next/router'
import routeType from '../actions/routeType'
import { genRoute, userRoute } from './../../components/route/routeList'

const initialState = {
  routeName: null,
  path: null,
  RouterArr: genRoute,
}

const routeReducer = (state = initialState, action) => {
  switch (action.type) {
    case routeType.GOTO_ROUTE:
      action.payload && Router.push(action.payload)
      return {
        ...state,
        path: action.payload,
      }
    case routeType.USE_USER_ROUTE:
      return {
        ...state,
        RouterArr: userRoute,
      }
    case routeType.RESET_ROUTE:
      Router.push('/')
      return initialState
    default:
      return state
  }
}

export default routeReducer
