import userType from '../actions/userType'

const initialState = {
  name: null,
  uid: null,
  userType: null,
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case userType.SET_NAME_AND_UID:
      // console.log('userReducer', action.payload)
      return {
        ...state,
        name: action.payload.name,
        uid: action.payload.uid,
      }
    case userType.SET_UID:
      // console.log('userReducer', action.payload)
      return {
        ...state,
        uid: action.payload,
      }
    case userType.RESET_USER:
      return initialState

    default:
      return state
  }
}

export default userReducer
