import { combineReducers } from 'redux'
import userReducer from './userReduser'
import routeReducer from './routeReduser'

const rootReducer = combineReducers({
  userReducer,
  routeReducer,
})

export default rootReducer
