import React from 'react'
import { connect } from 'react-redux'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/styles'
import {
  Grid,
  Paper,
  Container,
  GridList,
  GridListTile,
  GridListTileBar,
  Button,
} from '@material-ui/core'
import Header from '../../components/Header'
import Avatar from '@material-ui/core/Avatar'
import { withRouter } from 'next/router'
import routeType from '../../redux/actions/routeType'

const useStyle = makeStyles(theme => {
  //console.log(theme.palette.background)

  return {
    test: {
      padding: theme.palette.background,
    },
    gridList: {
      width: '100%',
    },
    icon: {
      color: 'rgba(255, 255, 255, 0.54)',
    },
    avt: {
      width: '100%',
      height: '100%',
      position: 'relative',
      top: '0',
      boxShadow:
        '0 8px 12px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
    },
    button: {
      margin: theme.spacing(1),
    },
    input: {
      display: 'none',
    },
  }
})

const Profile = props => {
  //console.log('Profile', props)
  const {
    dispatch,
    userReducer: { uid },
  } = props
  const classes = useStyle()

  return (
    <React.Fragment>
      <Header />
      <div className='root'>
        <div
          style={{
            width: '100%',
            height: '45%',
            position: 'absolute',
            top: '0',
            zIndex: '-1',
            overflow: 'hidden',
          }}
        >
          <img
            src='./static/images/photo.jpg'
            alt=''
            style={{ width: '100%' }}
          />
        </div>
        <Container maxWidth='lg' style={{ marginTop: '100px' }}>
          <div className='im-profile'>
            <Avatar
              alt=''
              src='./static/images/dog.jpg'
              className={classes.avt}
            />
          </div>
          <Typography
            variant='h5'
            align='center'
            style={{
              textShadow: '2px 2px 4px #000000',
              color: '#fff',
              marginBottom: '30px',
            }}
          >
            สมทวย คงควรคอย
          </Typography>
          <Button
            variant='contained'
            className={classes.button}
            onClick={() =>
              dispatch({
                type: routeType.GOTO_ROUTE,
                payload: `/profile/updateProfile?photographId=${uid}`,
              })
            }
          >
            แก้ไขข้อมูล
          </Button>
          <Grid container style={{ background: '#fff' }} justify='center'>
            <Grid item xs={12} sm={4} style={{ padding: '15px' }}>
              <Paper square style={{ padding: '15px', marginBottom: '30px' }}>
                <Typography variant='h5' align='center' color='primary'>
                  ข้อมูลทั่วไป
                </Typography>
                <hr />
              </Paper>
              <Paper square style={{ padding: '15px' }}>
                <Typography variant='h5' align='center' color='primary'>
                  การรับงาน
                </Typography>
                <hr />
              </Paper>
            </Grid>
            <Grid item xs={12} sm={8} style={{ padding: '15px' }}>
              <Typography variant='h5' align='center' color='primary'>
                ผลงานที่ผ่านมา
              </Typography>
              <hr />
              <div>
                <GridList className={classes.gridList} cols={3}>
                  <GridListTile>
                    <img alt='' src='./static/images/dog.jpg' />
                    <GridListTileBar
                      title='wegweger'
                      subtitle={<span>by: egergergerg</span>}
                    ></GridListTileBar>
                  </GridListTile>
                  <GridListTile>
                    <img alt='' src='./static/images/dog.jpg' />
                    <GridListTileBar
                      title='wegweger'
                      subtitle={<span>by: egergergerg</span>}
                    ></GridListTileBar>
                  </GridListTile>
                  <GridListTile>
                    <img alt='' src='./static/images/dog.jpg' />
                    <GridListTileBar
                      title='wegweger'
                      subtitle={<span>by: egergergerg</span>}
                    ></GridListTileBar>
                  </GridListTile>
                </GridList>
              </div>
            </Grid>
          </Grid>
        </Container>
      </div>
    </React.Fragment>
  )
}

export default connect(state => state)(withRouter(Profile))
