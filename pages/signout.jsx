import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import routeType from '../redux/actions/routeType'
import { BounceLoader } from 'react-spinners'
import css from '@emotion/css'
import userType from './../redux/actions/userType'
import { signOutUser } from './../auth/authen'

const override = css`
  display: block;
  margin: 0 auto;
`

const SignOut = props => {
  const { dispatch } = props
  //console.log('SignOut', props)
  useEffect(() => {
    console.log('object')
    localStorage.clear()
    signOutUser().then(res => {
      setstate(false)
      if (res) {
        dispatch({
          type: routeType.RESET_ROUTE,
        })
        dispatch({
          type: userType.RESET_USER,
        })
        dispatch({
          type: routeType.GOTO_ROUTE,
          payload: `/`,
        })
      }
    })
  }, [dispatch])

  const [isLoad, setstate] = useState(true)
  return (
    <div>
      <div className='sweet-loading' style={{ paddingTop: '200px' }}>
        <BounceLoader
          css={override}
          sizeUnit='px'
          size={150}
          color='#48BFA3'
          loading={isLoad}
        />
      </div>
    </div>
  )
}

export default connect(state => state)(SignOut)
