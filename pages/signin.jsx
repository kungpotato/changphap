import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import routeType from '../redux/actions/routeType'
import { BounceLoader } from 'react-spinners'
import css from '@emotion/css'
import ResponsiveContainerGrid from './../components/signIn/ResponsiveContainerGrid'
import { Grid, TextField, Button } from '@material-ui/core'
import ResponsiveCard from './../components/signIn/ResponsiveCard'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import {
  FacebookLoginButton,
  GoogleLoginButton,
} from 'react-social-login-buttons'
import CardActions from '@material-ui/core/CardActions'
import * as Yup from 'yup'
import { Formik } from 'formik'
import {
  signIn,
  getsUserFromGoogle,
  getsUserFromFacebook,
} from '../auth/authen'
import { CheckUser } from './../auth/authen'
import userType from '../redux/actions/userType'

const override = css`
  display: block;
  margin: 0 auto;
`

const SignIn = props => {
  const {
    dispatch,
    userReducer: { uid },
  } = props
  // console.log('SignIn', props)
  useEffect(() => {
    if (uid) {
      dispatch({
        type: routeType.GOTO_ROUTE,
        payload: `/profile?photographId=${uid}`,
      })
    }
  }, [dispatch, uid])

  const [isOpen, setIsOpen] = useState(true)
  const inputs = {
    email: '',
    password: '',
  }

  const initUserSetup = (userid, displayName) => {
    dispatch({
      type: routeType.USE_USER_ROUTE,
    })
    dispatch({
      type: userType.SET_NAME_AND_UID,
      payload: {
        name: displayName,
        uid: userid,
      },
    })
    dispatch({
      type: routeType.GOTO_ROUTE,
      payload: `/profile?photographId=${userid}`,
    })
  }

  const signInUser = data => {
    signIn(data.email, data.password)
    CheckUser().then(user => {
      if (user) {
        initUserSetup(user.uid, user.displayName)
      }
    })
  }

  return (
    <div>
      <div
        className='sweet-loading'
        style={{ paddingTop: '200px', display: isOpen ? 'none' : 'block' }}
      >
        <BounceLoader
          css={override}
          sizeUnit='px'
          size={150}
          color='#48BFA3'
          loading={true}
        />
      </div>
      <ResponsiveContainerGrid>
        <Grid item xs={12}>
          <ResponsiveCard>
            <Formik
              render={props => (
                <InputFrom {...props} initUserSetup={initUserSetup} />
              )}
              initialValues={inputs}
              validationSchema={validationSchema}
              onSubmit={signInUser}
            />
          </ResponsiveCard>
        </Grid>
      </ResponsiveContainerGrid>
    </div>
  )
}

const validationSchema = Yup.object({
  email: Yup.string()
    .email('คุณใส่ email ไม่ถูกต้อง')
    .max(30, 'คุณใส่ตัวอักษรยาวเกินกำหนด')
    .required('กรูณาใส่ email'),
  password: Yup.string('')
    .min(6, 'รหัสผ่านต้องมีอย่างน้อย 8 ตัวอักษร')
    .max(30, 'คุณใส่ตัวอักษรยาวเกินกำหนด')
    .required('กรูณาใส่รหัสผ่าน'),
})

const InputFrom = props => {
  //console.log('InputFrom', props)
  const { handleSubmit, handleChange, errors, initUserSetup } = props
  return (
    <form onSubmit={handleSubmit}>
      <img
        src='./static/images/logo2.png'
        alt=''
        style={{
          width: '150px',
          cursor: 'pointer',
        }}
      />
      <CardHeader title='เข้าสู่ระบบ' subheader='SongNiw ยินดีต้อนรับ' />
      <CardContent>
        <Grid container>
          <Grid item xs={12} sm={6}>
            <FacebookLoginButton
              size='40px'
              style={{ fontSize: '15px' }}
              text='เข้าสู่ระบบด้วย Facebook'
              align='center'
              onClick={() =>
                getsUserFromGoogle().then(res => {
                  console.log('getsUserFromGoogle', res)
                  initUserSetup(res.user.uid, res.user.displayName)
                })
              }
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <GoogleLoginButton
              size='40px'
              style={{ fontSize: '15px' }}
              text='เข้าสู่ระบบด้วย Google'
              align='center'
              onClick={() =>
                getsUserFromFacebook().then(res => {
                  console.log('getsUserFromFacebook', res)
                  initUserSetup(res.user.uid, res.user.displayName)
                })
              }
            />
          </Grid>
        </Grid>
        <TextField
          name='email'
          label='Email'
          fullWidth
          autoFocus
          margin='normal'
          helperText={errors.email}
          onChange={handleChange}
          error={!!errors.email}
        />
        <TextField
          name='password'
          type='password'
          label='Password'
          fullWidth
          margin='normal'
          onChange={handleChange}
          helperText={errors.password}
          error={!!errors.password}
        />
      </CardContent>
      <CardActions>
        <Button color='secondary'>ลืมรหัสผ่าน</Button>
        <Button color='secondary'>ลงทะเบียนผู้ใช้</Button>
        <Button type='submit' color='primary'>
          เข้าสู่ระบบ
        </Button>
      </CardActions>
    </form>
  )
}

export default connect(state => state)(SignIn)
