import React from 'react'
import Header from '../components/Header'
import { Container } from '@material-ui/core'

const Setting = () => {
  return (
    <div>
      <Header />
      <Container maxWidth='xl' style={{ marginTop: '80px' }}>
        <h3>setting</h3>
      </Container>
    </div>
  )
}

export default Setting
