import React from 'react'
import Header from '../components/Header'
import SearchBox from '../components/home/SearchBox'
import HomeSec2 from '../components/home/HomeSec2'
import { connect } from 'react-redux'

// import { getsUserFromGoogle, getsUserFromFacebook } from '../auth/authen'

const Home = props => {
  // console.log('Index', props)
  const {
    userReducer: { name },
  } = props

  return (
    <React.Fragment>
      <Header />
      <SearchBox />
      <HomeSec2 />
    </React.Fragment>
  )
}

Home.getInitialProps = ({ store }) => {
  const setName = param => {
    store.dispatch({
      type: 'setName',
      payload: param,
    })
  }
  //setName('potao')
  return { setName }
}

export default connect(state => state)(Home)
