import React from 'react'
import { Provider } from 'react-redux'
import App, { Container } from 'next/app'
import '../styles/styles.scss'
import { ThemeProvider } from '@material-ui/styles'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import theme from '../theme'
import DateFnsUtils from '@date-io/date-fns'
import thLocale from 'date-fns/locale/th'
import { CheckUser } from './../auth/authen'
import withRedux from 'next-redux-wrapper'
import { createStore } from 'redux'
import rootReducer from '../redux/reducers'
import { getParameterByName } from '../helperFunction/helper'
import routeType from '../redux/actions/routeType'
import userType from '../redux/actions/userType'

const makeStore = (initialState, options) => {
  return createStore(rootReducer, initialState)
}

class MyApp extends App {
  handleFailProfileRoute = (userid, store) => {
    const photographId = getParameterByName(
      'photographId',
      window.location.href
    )

    if (window.location.pathname.includes('/profile')) {
      if (!photographId || userid != photographId) {
        store.dispatch({
          type: routeType.GOTO_ROUTE,
          payload: '/',
        })
      }
    }
  }

  componentDidMount() {
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) {
      jssStyles.parentNode.removeChild(jssStyles)
    }

    const { store } = this.props
    CheckUser().then(user => {
      if (user) {
        store.dispatch({
          type: routeType.USE_USER_ROUTE,
        })
        store.dispatch({
          type: userType.SET_NAME_AND_UID,
          payload: {
            name: user.displayName,
            uid: user.uid,
          },
        })
      } else {
        store.dispatch({
          type: routeType.GOTO_ROUTE,
          payload: '/',
        })
      }
    })

    const {
      userReducer: { uid },
    } = store.getState()
    this.handleFailProfileRoute(uid, store)
  }

  render() {
    const { Component, pageProps, store } = this.props
    return (
      <ThemeProvider theme={theme}>
        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={thLocale}>
          <Container>
            <Provider store={store}>
              <Component {...pageProps} />
            </Provider>
          </Container>
        </MuiPickersUtilsProvider>
      </ThemeProvider>
    )
  }
}

export default withRedux(makeStore)(MyApp)
