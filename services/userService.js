import Firebase from '../firebase'

const db = Firebase.firestore()

export const addNewUser = (
  username,
  password,
  firstName,
  lastName,
  email,
  phone,
  userType
) => {
  let res = new Promise((resolve, reject) => {
    db.collection('users')
      .add({
        username,
        password,
        name: {
          firstName,
          lastName,
        },
        email,
        phone,
        userType,
      })
      .then(function(docRef) {
        console.log('Document written with ID: ', docRef.id)
        resolve(docRef.id)
      })
      .catch(function(error) {
        console.error('Error adding document: ', error)
        reject(error)
      })
  })
  return res
}

export const getUserByEmail = email => {
  let res = new Promise((resolve, reject) => {
    db.collection('users')
      .where('email', '==', email)
      .get()
      .then(function(querySnapshot) {
        if (querySnapshot.empty) {
          resolve(null)
        } else {
          querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            //console.log(doc.id, ' => ', doc.data())
            resolve(doc.data())
          })
        }
      })
      .catch(function(error) {
        console.log('Error getting documents: ', error)
        reject(error.message)
      })
  })
  return res
}
